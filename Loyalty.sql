CREATE TABLE LOYALTY_NEW 
(
  LOYALTY_ID NUMBER NOT NULL 
, UCID NUMBER(20) 
, BANNER_ID NUMBER(20) NOT NULL 
, FIRST_NAME VARCHAR2(60) 
, LAST_NAME VARCHAR2(60) 
, EMAIL VARCHAR2(150) 
, PHONE VARCHAR2(20) 
, ADDRESS1 VARCHAR2(150) 
, ADDRESS2 VARCHAR2(150) 
, ADDRESS3 VARCHAR2(150) 
, CITY VARCHAR2(50) 
, STATE VARCHAR2(50) 
, POSTAL VARCHAR2(15) 
, LANGUAGE_PREF_CD NUMBER(6) 
, LOYALTY_CONSENT_DATE DATE 
, LOYALTY_CONSENT_SOURCE VARCHAR2(50) 
, CASL_EMAIL CHAR(1) NOT NULL 
, CASL_ADDRESS CHAR(1) NOT NULL 
, CASL_PHONE CHAR(1) NOT NULL 
, LOYALTY_EXP_DATE DATE 
, POINT_BAL_EXP NUMBER(20) 
, LAST_KNOWN_POINTS NUMBER(20) 
, LAST_KNOWN_POINTS_VALUE NUMBER(20) 
, ASSOCIATE_FLAG CHAR(1) 
, DISCOUNT_ELIGLIBILITY_FLAG CHAR(1) 
, ENROLL_TYPE VARCHAR2(20) 
, ENROLL_SOURCE VARCHAR2(20) 
, ENROLL_TS TIMESTAMP 
, ENROLL_LAST_UPDATED_SOURCE VARCHAR2(50) 
, ENROLL_LAST_UPDATE_TS TIMESTAMP 
, PRIMARY CHAR(1) 
, LAST_EMAIL_PHONE_UPDATE_DATE TIMESTAMP 
, CHARITY_FLAG CHAR(1) 
, OFFLINE_ENROLL_FLAG CHAR(1) 
, ACCOUNT_STATUS VARCHAR2(20) 
, CONSTRAINT LOYALTY_NEW_PK PRIMARY KEY 
  (
    LOYALTY_ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX LOYALTY_NEW_PK ON LOYALTY_NEW (LOYALTY_ID ASC) 
  )
  ENABLE 
);

COMMENT ON COLUMN LOYALTY_NEW.LOYALTY_ID IS 'Primary Key and Unique';

COMMENT ON COLUMN LOYALTY_NEW.BANNER_ID IS 'Not null';

COMMENT ON COLUMN LOYALTY_NEW.CASL_EMAIL IS 'Not null';

COMMENT ON COLUMN LOYALTY_NEW.CASL_ADDRESS IS 'Not null';

COMMENT ON COLUMN LOYALTY_NEW.CASL_PHONE IS 'Not null';


CREATE TABLE LOYALTY_EXT_ID_TYPE 
(
  LOYALTY_ID NUMBER 
, ID_TYPE_DESC VARCHAR2(100) 
, ID_TYPE_CODE VARCHAR2(50) 
, LAST_MOD_DATE TIMESTAMP 
, LAST_MOD_ID VARCHAR2(20) 
, PRIMARY_CARD_HOLDER CHAR(1) 
, AUTHORIZED_SEC_CARD_HOLDER CHAR(1) 
, ID NUMBER DEFAULT UCID."ISEQ$$_314931".nextval NOT NULL 
, BILLING_CYCLE NUMBER 
, CARD_OPEN_DATE DATE 
, CARD_CLOSE_DATE DATE 
, EMPLOYEE_FLAG CHAR(1) 
, CREDIT_ACCOUNT NUMBER 
) 
LOGGING 
TABLESPACE SYSTEM 
PCTFREE 10 
PCTUSED 40 
INITRANS 1 
STORAGE 
( 
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1 
  MAXEXTENTS UNLIMITED 
  FREELISTS 1 
  FREELIST GROUPS 1 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;

ALTER TABLE LOYALTY_EXT_ID_TYPE
ADD CONSTRAINT LOYALTY_EXT_ID_TYPE_FK1 FOREIGN KEY
(
  LOYALTY_ID 
)
REFERENCES LOYALTY_NEW
(
  LOYALTY_ID 
)
ENABLE;

COMMENT ON COLUMN LOYALTY_EXT_ID_TYPE.LOYALTY_ID IS 'Foreign Key table loyalty_new';

CREATE TABLE LOYALTY_PARTNER 
(
  LOYALTY_PTN_NBR VARCHAR2(20) 
, LOYALTY_ID VARCHAR2(20) 
, LOYALTY_PGM_ID VARCHAR2(20) 
, PARTNER_ID VARCHAR2(20) 
);

CREATE TABLE LOYALTY_CHARITY 
(
  LOYALTY_ID NUMBER NOT NULL 
, CHARITY_LONG_EDESC VARCHAR2(150) 
, CHARITY_LONG_FDESC VARCHAR2(150) 
, CHARITY_POS_EDESC VARCHAR2(150) 
, CHARITY_POS_FDESC VARCHAR2(150) 
, CHARITY_START_DATE DATE 
, CHARITY_END_DATE DATE 
, GL_CODE NUMBER 
, STATUS_CODE CHAR(1) 
, CONSTRAINT LOYALTY_CHARITY_PK PRIMARY KEY 
  (
    LOYALTY_ID 
  )
  ENABLE 
);

COMMENT ON COLUMN LOYALTY_CHARITY.LOYALTY_ID IS 'Primary Key and Unique';

COMMENT ON COLUMN LOYALTY_CHARITY.CHARITY_LONG_EDESC IS 'English Long Desc';

COMMENT ON COLUMN LOYALTY_CHARITY.CHARITY_LONG_FDESC IS 'French Long Desc';

COMMENT ON COLUMN LOYALTY_CHARITY.CHARITY_POS_EDESC IS 'English charity code for Register';

COMMENT ON COLUMN LOYALTY_CHARITY.CHARITY_POS_FDESC IS 'French charity code for Register';

CREATE TABLE LOYALTY_PIN 
(
  LOYALTY_ID NUMBER NOT NULL 
, PIN NUMBER NOT NULL 
, CREATE_DATE DATE 
, UPDATE_DATE DATE 
, PIN_INVALID_COUNTER NUMBER 
, PWP_BLOCK_FLAG CHAR(1) 
);


CREATE TABLE LOYALTY_PRODUCT_CATALOG 
(
  SKU NUMBER NOT NULL 
, ITEM_DESC VARCHAR2(60) 
, BRAND NUMBER(7) 
, GMM NUMBER 
, DMM NUMBER 
, BUYER NUMBER 
, DEPT NUMBER(4) 
, CLASS NUMBER(3) 
, STYLE NUMBER 
, MFG_NO NUMBER(4) 
, UPDATE_DATE DATE 
, LAST_SENT_DATE DATE 
, LOYALTY_ELIG CHAR(1) 
);

CREATE TABLE LOYALTY_ROLLOUT_SCHEMA 
(
  STORE_NO NUMBER 
, ROLLOUT_DATE DATE 
);

CREATE TABLE LOYALTY_TENDER_XREF 
(
  TENDER_CODE NUMBER NOT NULL 
, CHANNEL VARCHAR2(20) NOT NULL 
, TENDER_DESC VARCHAR2(20) NOT NULL 
, LOYALTY_TENDER_DESC VARCHAR2(20) NOT NULL 
);

COMMENT ON COLUMN LOYALTY_TENDER_XREF.TENDER_CODE IS 'Tender code as sent by 
channel';

COMMENT ON COLUMN LOYALTY_TENDER_XREF.CHANNEL IS 'POS, OMS, Digital';

COMMENT ON COLUMN LOYALTY_TENDER_XREF.TENDER_DESC IS 'Long description of tender';

COMMENT ON COLUMN LOYALTY_TENDER_XREF.LOYALTY_TENDER_DESC IS 'Tender description as 
mapped with Merkle';





